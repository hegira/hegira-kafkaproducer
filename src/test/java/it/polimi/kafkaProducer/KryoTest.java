/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.kafkaProducer;

import static org.junit.Assert.*;
import it.polimi.hegira.hegira_metamodel.sync.MsgWrapper;
import it.polimi.hegira.hegira_metamodel.sync.Operations;
import it.polimi.hegira.kafkaProducer.serializator.KryoSerializator;

import org.junit.BeforeClass;
import org.junit.Test;

public class KryoTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void test() {
		MsgWrapper msg = new MsgWrapper("1", Operations.INSERT);
		msg.setContent("Ciao".getBytes());
		
		KryoSerializator serializator = new KryoSerializator(msg.getClass());
		byte[] bytes = serializator.serialize(msg);
		
		MsgWrapper object = (MsgWrapper) serializator.deserialize(bytes);
		assertEquals(msg.getOpId(), object.getOpId());
		assertEquals(msg.getOperation(), object.getOperation());
		assertEquals(new String(msg.getContent()), new String(object.getContent()));
	}

}
