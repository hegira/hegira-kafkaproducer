/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.kafkaProducer.discovery;

import java.io.Closeable;
import java.io.IOException;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.imps.CuratorFrameworkState;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.utils.CloseableUtils;
import org.junit.runners.model.InitializationError;
import org.apache.curator.x.discovery.ServiceDiscovery;
import org.apache.curator.x.discovery.ServiceDiscoveryBuilder;
import org.apache.curator.x.discovery.ServiceInstance;
import org.apache.curator.x.discovery.UriSpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marco Scavuzzo
 *
 */
public class DiscoveryServer implements Closeable {
	private transient static Logger log = LoggerFactory.getLogger(DiscoveryServer.class);
	private static DiscoveryServer instance;
	private static Object instance_lock = new Object();
	private static CuratorFramework client;
	
	private static final String PATH = "/hegira/discovery";
	private static final String serviceName = "kafkaProducers";
	private static String connectString;
	
	private ServiceDiscovery<String> serviceDiscovery;
	private ServiceInstance<String> thisInstance;
	
	private DiscoveryServer(){
		if(connectString==null){
			throw new IllegalStateException();
		}
		
		client = CuratorFrameworkFactory.newClient(connectString,
				new ExponentialBackoffRetry(1000, 3));
		
	}
	
	public static DiscoveryServer getInstance() throws Exception{
		if(!isInitialized()){
			throw new InitializationError(DiscoveryServer.class.getCanonicalName()
					+" must first be initialized!");
		}
		
		synchronized(instance_lock){
			instance = instance==null ? new DiscoveryServer() : instance;
			if(!isConnected())
				instance.connect();
		}
		return instance;
	}
	
	public static synchronized void init(String connectString){
		if(connectString==null){
			throw new IllegalArgumentException();
		}
		DiscoveryServer.connectString = connectString;
	}
	
	private static boolean isInitialized(){
		return connectString!=null;
	}
	
	private static boolean isConnected(){
		if(client==null)
			throw new IllegalStateException("CuratorFrameworkClient is null!");
		return client.getState().equals(CuratorFrameworkState.STARTED);
	}
	
	private void connect() throws Exception{
		if(isConnected()){
			log.warn("{} - Already connected.",
					Thread.currentThread().getName());
			return;
		}
		
		while(!isConnected())
			client.start();
	}
	
	public void disconnect(){
		if(!isConnected())
			return;
		
		try {
			close();

			log.debug("{} - Disconnected.",
					Thread.currentThread().getName());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public String addService(String address, int port) throws Exception{
		UriSpec uriSpec = new UriSpec("{address}:{port}");
		
		thisInstance = ServiceInstance.<String>builder()
				.name(serviceName)
				.port(port)
				.uriSpec(uriSpec)
				.build();
		
		// if you mark your payload class with @JsonRootName the provided JsonInstanceSerializer will work
        //JsonInstanceSerializer<String> serializer = new JsonInstanceSerializer<String>(String.class);
		
		serviceDiscovery = ServiceDiscoveryBuilder.builder(String.class)
				.client(client)
				.basePath(PATH)
				.thisInstance(thisInstance)
				.build();
		
		//serviceDiscovery.registerService(thisInstance);
		serviceDiscovery.start();
		log.debug("Adding service to ZooKeeper: {}",thisInstance);
		
		if(thisInstance!=null)
			return thisInstance.getId();
		else
			return null;
	}

	@Override
	public void close() throws IOException {
		CloseableUtils.closeQuietly(serviceDiscovery);
		CloseableUtils.closeQuietly(client);
	}
}
