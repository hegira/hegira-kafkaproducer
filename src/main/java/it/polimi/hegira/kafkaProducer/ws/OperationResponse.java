/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.kafkaProducer.ws;

/**
 * @author Marco Scavuzzo
 *
 */
public class OperationResponse {
	public enum QueryType {
		INSERT,UPDATE,DELETE
	}
	
	public enum OperationResult{
		SUCCESS, FAIL
	}
	
	private String primaryKey, tblName;
	private boolean SRC, DST;
	private QueryType qType;
	private OperationResult result;
	private long timestamp;
	
	public OperationResponse(String primaryKey, String tblName, boolean sRC, boolean dST, QueryType qType,
			OperationResult result) {
		this.primaryKey = primaryKey;
		this.tblName = tblName;
		SRC = sRC;
		DST = dST;
		this.qType = qType;
		this.result = result;
	}
	
	public OperationResponse(String primaryKey, String tblName, boolean sRC, boolean dST, String qType,
			OperationResult result, long timestamp) {
		this.primaryKey = primaryKey;
		this.tblName = tblName;
		SRC = sRC;
		DST = dST;
		this.result = result;
		this.timestamp=timestamp;
		
		if(qType.equals(QueryType.INSERT.name())){
			this.qType = QueryType.INSERT;
		}else if(qType.equals(QueryType.UPDATE.name())) {
			this.qType = QueryType.UPDATE;
		}else if(qType.equals(QueryType.DELETE.name())) {
			this.qType = QueryType.DELETE;
		}else{
			throw new IllegalArgumentException("Wrong query type!");
		}
	}

	public String getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}

	public String getTblName() {
		return tblName;
	}

	public void setTblName(String tblName) {
		this.tblName = tblName;
	}

	public boolean isSRC() {
		return SRC;
	}

	public void setSRC(boolean sRC) {
		SRC = sRC;
	}

	public boolean isDST() {
		return DST;
	}

	public void setDST(boolean dST) {
		DST = dST;
	}

	public QueryType getqType() {
		return qType;
	}

	public void setqType(QueryType qType) {
		this.qType = qType;
	}

	public OperationResult getResult() {
		return result;
	}

	public void setResult(OperationResult result) {
		this.result = result;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "OperationResponse [primaryKey=" + primaryKey + ", tblName=" + tblName + ", SRC=" + SRC + ", DST=" + DST
				+ ", qType=" + qType + ", result=" + result + ", timestamp="+timestamp+"]";
	}
}
