/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.kafkaProducer.ws;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marco Scavuzzo
 */
//package-visibility
class WebServer {
	private int port = 9779;
	private String contextPath = "/";
	private Server jettyServer = null;
	private transient Logger log = LoggerFactory.getLogger(WebServer.class);
	
	protected WebServer(String contextPath, int port){
		if(port>1024 && port<99999)
			this.port=port;
		if(contextPath!=null)
			this.contextPath=contextPath;
		
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath(this.contextPath);
		
		jettyServer = new Server(this.port);
		jettyServer.setHandler(context);
		
		ServletHolder jerseyServlet = context.addServlet(
	             org.glassfish.jersey.servlet.ServletContainer.class, "/*");
	    jerseyServlet.setInitOrder(0);
	 
	    // Tells the Jersey Servlet which REST service/class to load.
        jerseyServlet.setInitParameter(
           "jersey.config.server.provider.classnames",
           OperationsResponseEndpoint.class.getCanonicalName());
        
        //jerseyServlet.setInitParameter(
        //        "com.sun.jersey.api.json.POJOMappingFeature",
        //        "true");
        
        jerseyServlet.setInitParameter("jersey.config.server.provider.packages",
        		"com.jersey.jaxb,com.fasterxml.jackson.jaxrs.json");
        //Jackson Marshalling and Unmarshalling. Uncomment to activate (also pom.xml)
        /*jerseyServlet.setInitParameter("jersey.config.disableMoxyJson.server",
        		"true");
        jerseyServlet.setInitParameter("jersey.config.server.provider.packages",
        		"it.polimi.hegira.kafkaProducer.ws;org.codehaus.jackson.jaxrs");*/
	}
	
	protected boolean start(){
		if(jettyServer==null){
			log.error("Server not initialized!");
			throw new IllegalStateException();
		}
		
		try {
			jettyServer.start();
			log.debug("Jetty Server started");
			//jettyServer.join();
			//log.debug("Jetty Server joined");
		} catch (InterruptedException e) {
			log.error("Failed to join", e);
			return false;
		} catch (Exception e1) {
			log.error("Failed to start", e1);
			return false;
		}
		
		return true;
	}
	
	protected boolean close(){
		if(jettyServer==null){
			log.error("Server not initialized!");
			throw new IllegalStateException();
		}
		try {
			jettyServer.stop();
			jettyServer.destroy();
			return true;
		} catch (Exception e) {
			log.error("Couldn't stop the server or destroy it",e);
			return false;
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		close();
	}
}
