/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.kafkaProducer.ws;

import java.util.UUID;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.hegira.kafkaProducer.ws.OperationResponse.OperationResult;

/**
 * @author Marco Scavuzzo
 *
 */
@Path("/response")
public class OperationsResponseEndpoint {
	private transient Logger log = LoggerFactory.getLogger(OperationsResponseEndpoint.class);
	
	@GET
    @Path("test")
    @Produces(MediaType.TEXT_PLAIN)
    public String test() {
        return "Alive!";
    }
	
	@POST
	@Path("ack")
	@Produces(MediaType.APPLICATION_JSON)
	public Response ack(@QueryParam("opId") UUID opId,
			@QueryParam("pk") String pk, 
			@QueryParam("tblName") String tblName,
			@QueryParam("src") boolean src, 
			@QueryParam("dst") boolean dst, 
			@QueryParam("opType") String opType,
			@QueryParam("timestamp") long timestamp) {
		
		OperationResponse res = 
				new OperationResponse(pk, tblName, src, dst, opType, OperationResult.SUCCESS, timestamp);
		OperationResponseFuture responseFuture = OperationDispatcher.getInstance().removeOperationResponseFuture(opId, pk, timestamp);
		if(responseFuture!=null){
			responseFuture.put(res);
			//OperationResponseRes status = new OperationResponseRes();
			//status.setStatus(Status.SUCCESS);
			//return status;
			return Response.status(Status.OK).entity("response").build();
		}else{
			//OperationResponseRes status = new OperationResponseRes();
			//status.setStatus(Status.FAIL);
			//return status;
			log.error("Embedded server error. Couldn't find responseFuture for operation {}/{}/{} in OperationDispatcher",
					opId, pk, timestamp);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("response").build();
		}
	}
	
	@POST
	@Path("nack")
	@Produces(MediaType.APPLICATION_JSON)
	public Response nack(@QueryParam("opId") UUID opId,
			@QueryParam("pk") String pk, 
			@QueryParam("tblName") String tblName,
			@QueryParam("src") boolean src, 
			@QueryParam("dst") boolean dst, 
			@QueryParam("opType") String opType,
			@QueryParam("timestamp") long timestamp) {
		
		OperationResponse res = 
				new OperationResponse(pk, tblName, src, dst, opType, OperationResult.FAIL, timestamp);
		OperationResponseFuture responseFuture = OperationDispatcher.getInstance().removeOperationResponseFuture(opId, pk, timestamp);
		if(responseFuture!=null){
			responseFuture.put(res);
			//OperationResponseRes status = new OperationResponseRes();
			//status.setStatus(Status.SUCCESS);
			//return status;
			return Response.status(Status.OK).entity("response").build();
		}else{
			//OperationResponseRes status = new OperationResponseRes();
			//status.setStatus(Status.FAIL);
			//return status;
			log.error("Embedded server error. Couldn't find responseFuture for operation {}/{}/{} in OperationDispatcher",
					opId,pk,timestamp);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("response").build();
		}
	}
	
}
