/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.kafkaProducer.ws;

import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marco Scavuzzo
 *
 */
public class OperationResponseFuture implements Future<OperationResponse> {
	private final CountDownLatch latch = new CountDownLatch(1);
	private OperationResponse value;
	private UUID opId;
	private transient Logger log = LoggerFactory.getLogger(OperationResponseFuture.class);
	
	public OperationResponseFuture(UUID opId){
		this.opId = opId;
	}
	
	@Override
	public boolean cancel(boolean mayInterruptIfRunning) {
		return false;
	}

	@Override
	public OperationResponse get() throws InterruptedException, ExecutionException {
		latch.await();
		return value;
	}

	@Override
	public OperationResponse get(long timeout, TimeUnit unit)
			throws InterruptedException, ExecutionException, TimeoutException {
		
		if (latch.await(timeout, unit)) {
            return value;
        } else {
            throw new TimeoutException();
        }
	}

	@Override
	public boolean isCancelled() {
		return false;
	}

	@Override
	public boolean isDone() {
		return latch.getCount() == 0;
	}

	public void put(OperationResponse result) {
        this.value = result;
        latch.countDown();
	}
	
	public UUID getOpId(){
		return opId;
	}
}
