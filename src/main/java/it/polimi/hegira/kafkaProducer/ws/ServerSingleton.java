/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.kafkaProducer.ws;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.hegira.kafkaProducer.utils.Util;

/**
 * @author Marco Scavuzzo
 *
 */
public class ServerSingleton {
	private transient static Logger log = LoggerFactory.getLogger(ServerSingleton.class);
	
	private static ServerSingleton instance = null;
	private static Boolean instance_lock = true;
	
	private static int port=-1;
	private static String contextPath = null;
	
	private static WebServer ws;
	private static boolean started = false;
	
	private ServerSingleton(){
		ws = new WebServer(contextPath, port);
	}
	
	public static ServerSingleton getInstance(){
		synchronized(instance_lock){
			instance = instance==null ? new ServerSingleton() : instance;
		}
		return instance;
	}
	
	public static synchronized void init(String path, int sPort){
		port=sPort;
		contextPath=path;
	}
	
	public synchronized boolean start(){
		if(ws!=null && !started){
			started=ws.start();
			log.info("Server started: {}/{}:{}",
					Util.getIpAddress(),
					contextPath, port);
		}else{
			log.error("Already started!");
		}
		return started;
	}
	
	public synchronized void stop(){
		if(ws!=null && started){
			log.info("Stopping Server: {}/{}:{}",
					Util.getIpAddress(),
					contextPath, port);
			try {Thread.sleep(1000);}catch(InterruptedException e){}
			ws.close();
		}else{
			log.error("Already stopped!");
		}
	}
}
