/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.kafkaProducer.ws;

import java.util.LinkedList;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marco Scavuzzo
 *
 */
public class OperationDispatcher {
	private ConcurrentHashMap<String, Queue<OperationResponseFuture>> futuresMap;
	private transient Logger log = LoggerFactory.getLogger(OperationDispatcher.class);
	
	private OperationDispatcher(){
		futuresMap = new ConcurrentHashMap<String, Queue<OperationResponseFuture>>();
	}
	
	private static class OperationDispatcherHolder {
		private static final OperationDispatcher instance = new OperationDispatcher();
	}
	
	public static OperationDispatcher getInstance(){
		return OperationDispatcherHolder.instance;
	}
	
	public synchronized boolean addOperationResponseFuture(UUID opId, String pk, long timestamp, OperationResponseFuture future){
		String key=getFuturesMapKey(opId, pk, timestamp);
		Queue<OperationResponseFuture> respFutures = futuresMap.get(key);
		if(respFutures==null)
			respFutures=new LinkedList<OperationResponseFuture>();
		respFutures.offer(future);
		futuresMap.put(key, respFutures);
		return true;
	}
	
	public synchronized OperationResponseFuture removeOperationResponseFuture(UUID opId, String pk, long timestamp){
		String key=getFuturesMapKey(opId, pk, timestamp);
		Queue<OperationResponseFuture> respFutures = futuresMap.get(key);
		if(respFutures==null){
			log.error("{} - No response future was ever stored for {}",
					Thread.currentThread().getName(),
					key);
			return null;
		}
		if(respFutures.isEmpty()){
			log.debug("{} - No response future is stored at the moment for {}",
					Thread.currentThread().getName(),
					key);
			return null;
		}
		
		OperationResponseFuture response = respFutures.poll();
		if(respFutures.isEmpty()){
			futuresMap.remove(key);
		}
		return response;
	}
	
	private String getFuturesMapKey(UUID opId, String pk, long timestamp){
		return opId.toString()+pk+timestamp;
	}
}
