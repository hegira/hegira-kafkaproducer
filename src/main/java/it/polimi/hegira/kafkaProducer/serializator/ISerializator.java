/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.kafkaProducer.serializator;

/**
 * Standard interface for serializators.
 * @author Marco Scavuzzo
 *
 */
public interface ISerializator {
	/**
	 * Serializes the given object into an array of bytes.
	 * @param message The object to serialize.
	 * @return The byte array.
	 */
	public byte[] serialize(Object message);
	
	/**
	 * Deserializes the given byte array into an object.
	 * @param binary The byte array.
	 * @return The deserialized object.
	 */
	public Object deserialize(byte[] binary);
}
