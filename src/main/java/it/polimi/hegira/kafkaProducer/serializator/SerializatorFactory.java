/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.kafkaProducer.serializator;

import it.polimi.hegira.hegira_metamodel.sync.MsgWrapper;
import it.polimi.hegira.kafkaProducer.utils.Constants;

/**
 * 
 * @author Marco Scavuzzo
 *
 */
public class SerializatorFactory {

	/**
	 * Gets one of the supported serializers.
	 * @param name The name of the serializer.
	 * @return The serializator if supported or <code>null</code>.
	 */
	public ISerializator getSerializator(String name){
		switch (name) {
		case Constants.Serializators.java:
			return new JavaSerializator();
		case Constants.Serializators.kryo:
			return new KryoSerializator(MsgWrapper.class);

		default:
			return null;
		}
	}
}
