/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.kafkaProducer.serializator;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

/**
 * Kryo safe serializator.
 * @author Marco Scavuzzo
 *
 */
public class KryoSerializator implements ISerializator {
	private Kryo kryo;
	private Class<?> type;
	
	/**
	 * Instantiate a Kryo (de)serializator specific for the type of object that needs to be
	 * (de)serialized.
	 * 
	 * Usage example:
	 * <code>
	 * </br>
	 * String type = obj.getClass().getCanonicalName();</br>
	 * KryoSerializer kryoSerializer = new KryoSerializer(Class.forName(type));</br>
	 * byte[] serialized = kryoSerializer.serialize(obj);</br>
	 * Object deserialized = kryoSerializer.deserialize(serialized);</br>
	 * </code>
	 */
	public KryoSerializator(Class<?> type) {
		kryo = new Kryo();
		this.type = type;
	}

	@Override
	public byte[] serialize(Object message) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Output output = new Output(baos);
		//baos.flush();
		kryo.writeObjectOrNull(output, message, message.getClass());
		output.close();
		byte[] byteArray = baos.toByteArray();
		return byteArray;
	}

	@Override
	public Object deserialize(byte[] binary) {
		Input input = new Input(new ByteArrayInputStream(binary));
		Object readObject = kryo.readObjectOrNull(input, type);
		input.close();
		return readObject;
	}

}
