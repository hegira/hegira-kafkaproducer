/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.kafkaProducer.productionLayer;

import it.polimi.hegira.hegira_metamodel.sync.MsgWrapper;
import it.polimi.hegira.hegira_metamodel.sync.Operations;
import it.polimi.hegira.kafkaProducer.discovery.DiscoveryServer;
import it.polimi.hegira.kafkaProducer.serializator.KryoSerializator;
import it.polimi.hegira.kafkaProducer.utils.Util;
import it.polimi.hegira.kafkaProducer.ws.OperationDispatcher;
import it.polimi.hegira.kafkaProducer.ws.OperationResponseFuture;
import it.polimi.hegira.kafkaProducer.ws.ServerSingleton;

import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marco Scavuzzo
 * @param <OperationResponse>
 *
 */
public class KafkaProducer<OperationResponse> {
	private transient Logger log = LoggerFactory.getLogger(KafkaProducer.class);
	private String topic;
	org.apache.kafka.clients.producer.KafkaProducer<String, byte[]> kafkaProducer;
	private boolean wsStarted = false;
	protected String instanceId;
	
	/**
	 * Creates a new Kafka Producer.
	 * @param kafkaConnectString kafka address and port (e.g., localhost:9092)
	 * @param topic The topic for this specific Producer.
	 */
	public KafkaProducer(String kafkaConnectString, String topic) {
		this.topic = topic;
		Properties props=new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,kafkaConnectString);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,ByteArraySerializer.class.getName());
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		
		kafkaProducer = new org.apache.kafka.clients.producer.KafkaProducer<String, byte[]>(props);
		log.debug("Connected to Kafka");
	}
	
	public KafkaProducer(String kafkaConnectString, 
			String topic, 
			String serverPath,
			int port, String zkConnectString) throws Exception {
		this(kafkaConnectString,topic);
		log.debug("Connecting Jersey");
		ServerSingleton.init(serverPath, port);
		wsStarted = ServerSingleton.getInstance().start();
		DiscoveryServer.init(zkConnectString);
		StringBuilder sb = new StringBuilder(Util.getIpAddress());
		if(serverPath!=null)
			sb.append("/"+serverPath);
		this.instanceId = DiscoveryServer.getInstance().addService(sb.toString(), port);
		if(!wsStarted){
			log.error("Throwing IllegalStateException");
			throw new IllegalStateException("The Web Server cannot be started!!");
		}
		log.debug("Jersey started");
	}

	/**
	 * Produces a message (blocking!) on the topic handled by the specific KafkaProducer.
	 * @param message
	 */
	public void send(byte[] message, String key){
		//If a valid partition number is specified that partition will be used when sending the record. 
		//If no partition is specified but a key is present a partition will be chosen using a hash of the key. 
		//If neither key nor partition is present a partition will be assigned in a round-robin fashion.
		try {
			RecordMetadata recordMetadata = kafkaProducer.send(new ProducerRecord<String, byte[]>(topic,key,message)).get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}
	
	//@Async
	protected Future<OperationResponse> response(UUID opId, String pk, long timestamp){
		OperationResponseFuture future = new OperationResponseFuture(opId);
		boolean result = OperationDispatcher.getInstance().addOperationResponseFuture(opId, pk, timestamp, future);
		return (Future<OperationResponse>) future;
	}
	
	/**
	 * Closes this producer connection to Kafka broker(s).
	 */
	public void close(){
		if(kafkaProducer!=null){
			kafkaProducer.close();
			kafkaProducer = null;
		}
		
		if(wsStarted){
			ServerSingleton.getInstance().stop();
			try {
				DiscoveryServer.getInstance().disconnect();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		System.out.println("Trying to send a message");
		KafkaProducer producer = new KafkaProducer("109.231.122.110:9092", "TryoutTopic");
		MsgWrapper msg = new MsgWrapper("1", Operations.INSERT);
		msg.setContent("Ciao".getBytes());
		KryoSerializator serializer = new KryoSerializator(msg.getClass());
		//JavaSerializator serializer = new JavaSerializator();
		producer.send(serializer.serialize(msg), msg.getOpId()+"");
		System.out.println("sent message");
	}
}
