/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.kafkaProducer.productionLayer;

import java.util.LinkedList;
import java.util.Queue;

public class Transaction<E> {
	private int txId;
	private Queue<E> operations;
	
	@SuppressWarnings("unused")
	private Transaction(){}
	
	public Transaction(int txId, Queue<E> operations) {
		if(txId<0 || txId>Integer.MAX_VALUE || operations==null)
			throw new IllegalArgumentException();
		this.txId = txId;
		this.operations = operations;
	}
	
	public Transaction(int txId) {
		if(txId<0 || txId>Integer.MAX_VALUE)
			throw new IllegalArgumentException();
		this.txId = txId;
		this.operations = new LinkedList<E>();
	}
	
	public int getTransactionId(){
		return txId;
	}
	
	public void addOperation(E op){
		operations.offer(op);
	}
	
	public E poll(){
		return operations.poll();
	}
	
	public E peek(){
		return operations.peek();
	}
	
	public boolean hasOperations() {
		return operations.size()>0;
	}
	
	public int size(){
		if(operations!=null){
			return operations.size();
		}
		
		return 0;
	}
	
}
