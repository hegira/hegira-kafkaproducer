/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.kafkaProducer.productionLayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.thrift.TException;
import org.apache.thrift.TSerializer;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.hegira.hegira_metamodel.Column;
import it.polimi.hegira.hegira_metamodel.Metamodel;
import it.polimi.hegira.hegira_metamodel.sync.MsgWrapper;
import it.polimi.hegira.hegira_metamodel.sync.Operations;
import it.polimi.hegira.kafkaProducer.serializator.JavaSerializator;
import it.polimi.hegira.kafkaProducer.ws.OperationResponse;

public class KafkaMetamodelProducer extends KafkaProducer<OperationResponse> {
	private transient static Logger log = LoggerFactory.getLogger(KafkaMetamodelProducer.class);
	private TSerializer thriftSerializer;
	private boolean isWSactive = false;
	
	public KafkaMetamodelProducer(String kafkaConnectString, String topic) {
		super(kafkaConnectString, topic);
		thriftSerializer = new TSerializer(new TBinaryProtocol.Factory());
	}
	
	public KafkaMetamodelProducer(String kafkaConnectString, 
			String topic,
			String serverPath,
			int port, 
			String zkConnectString) throws Exception {
		super(kafkaConnectString, topic, serverPath, port, zkConnectString);
		thriftSerializer = new TSerializer(new TBinaryProtocol.Factory());
		isWSactive=true;
	}
	
	public Future<OperationResponse>  insert(Metamodel message, boolean response) throws TException{
		UUID opId = UUID.randomUUID();
		MsgWrapper msg = new MsgWrapper(message.rowKey, Operations.INSERT);
		msg.setTxId(opId.toString());
		synchronized(thriftSerializer){
			msg.setContent(thriftSerializer.serialize(message));
		}
		msg.setTimestamp(System.currentTimeMillis());
		if(response && isWSactive){
			if(instanceId!=null){
				msg.setInstanceId(instanceId);
				//log.debug("Inserting entity with PK {} and instanceId {}",
				//		message.rowKey, instanceId);
			}else
				log.error("The Response Web Service is active, but the instanceId is null. Error in service discovery system");
		}
		
		JavaSerializator serializer = new JavaSerializator();
		send(serializer.serialize(msg), getEntityIdString(message));
		
		if(response && isWSactive){
			return response(opId, msg.getOpId(), msg.getTimestamp());
		}
		return null;
	}
	
	public ArrayList<Future<OperationResponse>> executeTransaction(Transaction<MsgWrapper> tx, boolean response) {
		final UUID txId = UUID.randomUUID();
		int opCounter=0;
		int txSize=tx.size();
		
		ArrayList<Future<OperationResponse>> responsesList 
			= new ArrayList<Future<OperationResponse>>(txSize);
		while(tx.hasOperations()){
			opCounter++;
			MsgWrapper op = tx.poll();
			op.setTxId(txId.toString());
			op.setTx(true);
			if(op.getTimestamp()<=0L)
				op.setTimestamp(System.currentTimeMillis());
			
			if(opCounter==txSize){
				op.setLastMsgInTx(true);
			}
			
			if(response && isWSactive){
				if(instanceId!=null)
					op.setInstanceId(instanceId);
				else
					log.error("The Response Web Service is active, but the instanceId is null. Error in service discovery system");
			}
			
			JavaSerializator serializer = new JavaSerializator();
			send(serializer.serialize(op), txId.toString());
			if(response && isWSactive){
				responsesList.add(response(txId, op.getOpId(), op.getTimestamp()));
			}
		}
		
		
		
		return responsesList;
	}
	
	public ArrayList<Future<OperationResponse>> insert(Transaction<Metamodel> tx, boolean response) throws TException {
		UUID opId = UUID.randomUUID();
		int opCounter=0;
		int txSize=tx.size();
		
		ArrayList<Future<OperationResponse>> responsesList 
			= new ArrayList<Future<OperationResponse>>(txSize);
		while(tx.hasOperations()){
			opCounter++;
			Metamodel op = tx.poll();
			MsgWrapper msg = new MsgWrapper(op.rowKey, Operations.INSERT);
			msg.setTx(true);
			msg.setTxId(opId.toString());
			msg.setTimestamp(System.currentTimeMillis());
			if(opCounter==txSize){
				msg.setLastMsgInTx(true);
			}
			synchronized(thriftSerializer){
				msg.setContent(thriftSerializer.serialize(op));
			}
			if(response && isWSactive){
				if(instanceId!=null)
					msg.setInstanceId(instanceId);
				else
					log.error("The Response Web Service is active, but the instanceId is null. Error in service discovery system");
			}
			
			JavaSerializator serializer = new JavaSerializator();
			send(serializer.serialize(msg), tx.getTransactionId()+"");
			
			if(response && isWSactive){
				responsesList.add(response(opId, msg.getOpId(), msg.getTimestamp()));
			}
		}
		
		
		return responsesList;
	}
	
	public Future<OperationResponse> update(Metamodel message, boolean response) throws TException{
		UUID opId = UUID.randomUUID();
		MsgWrapper msg = new MsgWrapper(message.rowKey, Operations.UPDATE);
		synchronized(thriftSerializer){
			msg.setContent(thriftSerializer.serialize(message));
		}
		msg.setTxId(opId.toString());
		msg.setTimestamp(System.currentTimeMillis());
		if(response && isWSactive){
			if(instanceId!=null){
				msg.setInstanceId(instanceId);
				//log.debug("Updating entity with PK {} and instanceId {}",
				//		message.rowKey, instanceId);
			}else
				log.error("The Response Web Service is active, but the instanceId is null. Error in service discovery system");
		}
		
		JavaSerializator serializer = new JavaSerializator();
		send(serializer.serialize(msg), getEntityIdString(message));
		
		if(response && isWSactive){
			return response(opId, msg.getOpId(), msg.getTimestamp());
		}
		return null;
	}
	
	public ArrayList<Future<OperationResponse>> update(Transaction<Metamodel> tx, boolean response) throws TException {
		UUID opId = UUID.randomUUID();
		int opCounter=0;
		int txSize=tx.size();

		ArrayList<Future<OperationResponse>> responsesList 
			= new ArrayList<Future<OperationResponse>>(txSize);
		while(tx.hasOperations()){
			Metamodel op = tx.poll();
			MsgWrapper msg = new MsgWrapper(op.rowKey, Operations.UPDATE);
			msg.setTx(true);
			msg.setTxId(opId.toString());
			msg.setTimestamp(System.currentTimeMillis());
			if(opCounter==txSize){
				msg.setLastMsgInTx(true);
			}
			synchronized(thriftSerializer){
				msg.setContent(thriftSerializer.serialize(op));
			}
			if(response && isWSactive){
				if(instanceId!=null)
					msg.setInstanceId(instanceId);
				else
					log.error("The Response Web Service is active, but the instanceId is null. Error in service discovery system");
			}
			
			JavaSerializator serializer = new JavaSerializator();
			send(serializer.serialize(msg), tx.getTransactionId()+"");
			
			if(response && isWSactive){
				responsesList.add(response(opId, msg.getOpId(), msg.getTimestamp()));
			}
		}
		
		return responsesList;
	}
	
	public Future<OperationResponse> delete(Metamodel message, boolean response) throws TException{
		UUID opId = UUID.randomUUID();
		MsgWrapper msg = new MsgWrapper(message.rowKey, Operations.DELETE);
		synchronized(thriftSerializer){
			msg.setContent(thriftSerializer.serialize(message));
		}
		msg.setTxId(opId.toString());
		msg.setTimestamp(System.currentTimeMillis());
		if(response && isWSactive){
			if(instanceId!=null){
				msg.setInstanceId(instanceId);
				//log.debug("Deleting entity with PK {} and instanceId {}",
				//		message.rowKey, instanceId);
			}else
				log.error("The Response Web Service is active, but the instanceId is null. Error in service discovery system");
		}
		
		JavaSerializator serializer = new JavaSerializator();
		send(serializer.serialize(msg), getEntityIdString(message));
		
		if(response && isWSactive){
			return response(opId, msg.getOpId(), msg.getTimestamp());
		}
		return null;
	}

	public ArrayList<Future<OperationResponse>> delete(Transaction<Metamodel> tx, boolean response) throws TException {
		UUID opId = UUID.randomUUID();
		int opCounter=0;
		int txSize=tx.size();

		ArrayList<Future<OperationResponse>> responsesList 
			= new ArrayList<Future<OperationResponse>>(txSize);
		while(tx.hasOperations()){
			Metamodel op = tx.poll();
			MsgWrapper msg = new MsgWrapper(op.rowKey, Operations.DELETE);
			msg.setTx(true);
			msg.setTxId(opId.toString());
			msg.setTimestamp(System.currentTimeMillis());
			if(opCounter==txSize){
				msg.setLastMsgInTx(true);
			}
			synchronized(thriftSerializer){
				msg.setContent(thriftSerializer.serialize(op));
			}
			if(response && isWSactive){
				if(instanceId!=null)
					msg.setInstanceId(instanceId);
				else
					log.error("The Response Web Service is active, but the instanceId is null. Error in service discovery system");
			}
			
			JavaSerializator serializer = new JavaSerializator();
			send(serializer.serialize(msg), tx.getTransactionId()+"");
			
			if(response && isWSactive){
				responsesList.add(response(opId, msg.getOpId(), msg.getTimestamp()));
			}
		}
		
		return responsesList;
	}
	
	@SuppressWarnings("unused")
	private int getEntityId(Metamodel entity) throws NumberFormatException{
		return Integer.parseInt(entity.getRowKey());
	}
	
	private String getEntityIdString(Metamodel entity) throws NumberFormatException{
		return entity.getRowKey();
	}
	
	public static void main(String[] args) {
		//KafkaMetamodelProducer producer = new KafkaMetamodelProducer("localhost:9092", "synch");
		log.debug("Starting KafkaMetamodelProducer");
		KafkaMetamodelProducer producer;
		try {
			producer = new KafkaMetamodelProducer("localhost:9092", "synch", null, 6060,"localhost:2181");
			log.debug("Generating metamodel entity");
			int opsNo = 1;
			final CountDownLatch latch = new CountDownLatch(opsNo);
			for(int i = 0; i<opsNo; i++){
				
				int randKey = ThreadLocalRandom.current().nextInt(1, 10000);
				String cf = "Test";
				final Column col = new Column();
				col.setColumnName("columnName");
				col.setColumnValue("columnValue".getBytes());
				col.setColumnValueType(String.class.getSimpleName());
				col.setIndexable(false);
				ArrayList<Column> cols = new ArrayList<Column>();
				cols.add(col);
				Map<String, List<Column>> kindcols = new HashMap<String, List<Column>>();
				kindcols.put(cf,
						cols);
				Metamodel mm = new Metamodel();
				mm.setPartitionGroup("partitionGroup");
				mm.setRowKey(""+randKey);
				mm.setColumns(kindcols);
				ArrayList<String> cfs = new ArrayList<String>();
				cfs.add(cf);
				mm.setColumnFamilies(cfs);
				boolean waitResponse = true;
				Future<OperationResponse> insertResponse = null; 
				try {
					insertResponse = producer.insert(mm, waitResponse);
				} catch (TException e) {
					e.printStackTrace();
				}
				log.debug("Sent message with PK "+randKey);
				if(waitResponse && insertResponse!=null){
					final Future<OperationResponse> fInsResp = insertResponse;
					new Thread(new Runnable() {
						
						@Override
						public void run() {
							log.debug("{} - Waiting for response",
									Thread.currentThread().getName());
							try {
								OperationResponse operationResponse = fInsResp.get();
								log.debug("{} - Got response: {}",
										Thread.currentThread().getName(),
										operationResponse);
							} catch (InterruptedException | ExecutionException e) {
								log.error("{} - Didn't get response. ",
										Thread.currentThread().getName(),
										e);
							}
							latch.countDown();
						}
					}).start();
					
				}
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			try {
				latch.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			producer.close();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}
